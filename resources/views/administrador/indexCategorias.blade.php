@extends('administrador.masterAdmin')

@section('content')
	<div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista de categorias</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
					@if(count($categorias)!=0)
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre </th>
                          <th>Editar</th>
                          <!--<th>Eliminar</th>-->
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($categorias as $categoria)
                        <tr>
                          <th scope="row">{{$categoria->id_categoria}}</th>
                          <td>{{$categoria->nombre}}</td>
                          <td><a href="{{route('administrador.categoria.edit',$categoria->id_categoria)}}" class="btn btn-primary">Actualizar</a></td>
                          <!--<td><a href="{{route('administrador.categoria.edit',$categoria->id_categoria)}}" class="btn btn-danger">Eliminar</a></td>-->
                        </tr>
                       @endforeach
                      </tbody>
                    </table>
                    {!! $categorias->render() !!}
                    @endif

                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
@stop
@section('scripts')
    @if(Session::has('mensaje'))
      <?php $mensaje=Session::get('mensaje');?>

      <script>
        var mensaje="<?php echo $mensaje; ?>";
        new PNotify({
          title: 'Correcto',
          text: mensaje,
          type: 'success'
      });
      </script>
      @endif
  @stop