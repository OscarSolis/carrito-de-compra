	@extends('administrador.masterAdmin')
	@section('content')
		<div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Registrar <small>Productos</small></h2>
                  </div>
                  <div class="x_content">
                   <br />
                   
                    {!! Form::open(['url' => 'administrador/producto','class'=>'form-horizontal form-label-left','files' => true]) !!}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Producto
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('nombre',null,['class'=>'form-control col-xs-12 col-md-7'])!!}
                        </div>
                      </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Categorias
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="id_categoria" class="form-control col-xs-12 col-md-7">
                            <option value="0">Seleccione una opcion</option>
                            @foreach($categorias as $categoria)
                              <option value="{{$categoria->id_categoria}}">{{$categoria->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    

                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Precio
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('precio',null,['class'=>'form-control col-xs-12 col-md-7'])!!}
                        </div>
                      </div>
  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cantidad
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('cantidad',null,['class'=>'form-control col-xs-12 col-md-7'])!!}
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Imagen
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::file('imagen',null,['class'=>'form-control col-xs-12 col-md-7'])!!}
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Descripción
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::textarea('descripcion',null,['class'=>'form-control col-xs-12 col-md-7'])!!}
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{url('administrador')}}" class="btn btn-primary">Cancelar</a>
                          <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
                      </div>

                    {!! Form::close() !!}
                  </div>
                </div>
            </div>
    	</div>
	@stop
	@section('scripts')
		@if(Session::has('mensaje'))
  		<?php $mensaje=Session::get('mensaje');?>

  		<script>
    		var mensaje="<?php echo $mensaje; ?>";
    		new PNotify({
    			title: 'Correcto',
    			text: mensaje,
    			type: 'success'
			});
    	</script>
    	@endif
	@stop