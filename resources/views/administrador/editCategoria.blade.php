@extends('administrador.masterAdmin')

@section('content')

	<div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Registrar <small>Categorias</small></h2>
                  </div>
                  <div class="x_content">
                   <br />
                   	{!! Form::model($categoria,['action' =>['CategoriaController@update',$categoria->id_categoria],'class'=>'form-horizontal form-label-left','method'=>'PUT']) !!}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Categoria
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('nombre',null,['class'=>'form-control col-xs-12 col-md-7'])!!}
                          
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <a href="{{url('administrador')}}" class="btn btn-primary">Cancelar</a>
                          <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
                      </div>

                    {!! Form::close() !!}
                  </div>
                </div>
            </div>
    	</div>
@stop