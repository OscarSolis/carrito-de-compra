@extends('administrador.masterAdmin')

@section('content')
	<div class="col-md-10 col-md-offset-1 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lista de productos</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
					@if(count($productos)!=0)
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre </th>
                          <th>precio</th>
                          <th>Cantidad</th>
                          <th>Descripcion</th>
                          <th>Imagen</th>
                          <th>Editar</th>
                          <!---<th>Eliminar</th>-->
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($productos as $producto)
                        <tr>
                          <th scope="row">{{$producto->id_producto}}</th>
                          <td>{{$producto->nombre}}</td>
                          <td>${{$producto->precio}}</td>
                          <td>{{$producto->cantidad}}</td>
                          <td>{{$producto->descripcion}}</td>
                          <td><img src="{{asset('imagenes')}}/{{$producto->imagen}}" width="50" height="50" class="img-responsive"></td>
                          <td><a href="{{route('administrador.producto.edit',$producto->id_producto)}}" class="btn btn-primary">Actualizar</a></td>
                          <!--<td><a href="{{route('administrador.categoria.edit',$producto->id_producto)}}" class="btn btn-danger">Eliminar</a></td>-->
                        </tr>
                       @endforeach
                      </tbody>
                    </table>
                    {!! $productos->render() !!}
                    @endif

                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
@stop
@section('scripts')
    @if(Session::has('mensaje'))
      <?php $mensaje=Session::get('mensaje');?>

      <script>
        var mensaje="<?php echo $mensaje; ?>";
        new PNotify({
          title: 'Correcto',
          text: mensaje,
          type: 'success'
      });
      </script>
      @endif
  @stop