<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentallela Alela! | </title>

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.custom.min.css')}}">
  </head>

  <body class="login">
  <br><br><br>   

  <div class="row">
    <div class="col-sm-12 col-md-4 col-md-offset-4">
      <div class="panel panel-default panel-l">
        <div class="panel-heading ">
          <h5 class="text-center text-primary">ACCESO AL SISTEMA</h5>
        </div>
        <div class="panel-body fondo">
          {!! Form::open(['url' => 'auth/login']) !!}
          @if(Session::has('mensaje'))
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            {{ Session::get('mensaje')}}
          </div>
          @endif

          <fieldset>
            <div class="row">
              <div class="col-sm-12 col-md-10 col-md-offset-1">
                <br>

                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-user" aria-hidden="true"></i>
                    </span>
                    {!! Form::text('email', null,['class'=>'form-control','id'=>'usuario', 'data-placement'=>'right']) !!}
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                    {!! Form::password('password',['class'=>'form-control','id'=>'password', 'data-placement'=>'right']) !!}
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <input type="submit" class="btn btn-block btn-login" value="ENTRAR">
                </div>
              </div>
            </div>
          </fieldset>
          {!!form::close()!!}
        </div>
      </div>
    </div>
  </div>
  
  
</div>
  </body>
</html>