@extends('front.masterFront')


@section('navbar')
	<nav class="navbar navbar-default " style="background-color: #222 !important;">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="{{url('/')}}">Pilo Store</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
              <li class="hidden">
                <a href="#page-top"></a>
              </li>
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias<span class="caret"></span></a>
                <ul class="dropdown-menu lista">
                	@foreach($categorias as $categoria)
					<li><a href="{{url('listarProductos',$categoria->id_categoria)}}">{{$categoria->nombre}}</a></li>
					<li role="separator" class="divider"></li>
                	@endforeach
                </ul>
                </li>
                  <!--<li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Categorias</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#team">Team</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>-->
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"> ( {{$cantiProductos}} )</i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{url('detalleCompra')}}">Ver detalle..</a></li>
                      </ul>
                   </li>
                  @if(\Auth::check())
                     <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-user fa-lg" aria-hidden="true"></i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{action('LoginController@logOut')}}">salir</a></li>
                          <li>
                            <a href="{{url('verCompras')}}">Ver Compras</a> 
                          </li>
                      </ul>
                      
                    @else
                      <li><a href="#modalLogin" class="portfolio-link" data-toggle="modal">Iniciar sesión</a></li>   
                      <li><a href="{{url('usuarios/create')}}">Registrarte</a></li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
@stop
@section('content')
    

  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb" style="background:none;">
          <li><a href="{{url('/')}}">Inicio</a></li>
          <li><a href="{{url('listarProductos',$producto->id_categoria)}}">{{$producto->categoria->nombre}}</a></li>
          <li class="active">{{$producto->nombre}}</li>
        </ol>
      </div>  
    </div>
    <br>
    <div class="row">
      <div class="col-md-6"> 
        <div class="panel panel-default" style="height:400px;">
         <br><br><br>
          <div class="panel-body">
            <img src="{{asset('imagenes')}}/{{$producto->imagen}}" class="img-responsive" style="margin:auto; height:200px !important; width:200px !important;" data-zoom-image="{{asset('imagenes')}}/{{$producto->imagen}}" id="imagen">
            </div>
        </div>
      </div>  
      <div class="col-md-6"> 
        <div class="panel panel-default" style="height:400px;">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <h3 class="text-center">{{$producto->nombre}}</h3>
                  <br>
                  <h5>Categoria: {{$producto->categoria->nombre}}</h5>
                  <br>
                  <h5>Precio: ${{$producto->precio}}</h5>
                  <br>
                  <h5>Descripción: {{$producto->descripcion}}</h5>
                  <br>
                  <h5>Cantidad disponible: {{$producto->cantidad}}</h5>
                </div> 
              </div>  
              <br>
              @if($producto->cantidad!=0)
              <div class="row">
                <div class="col-md-12">
                    {!!Form::open(['url'=>'addCart', 'method'=>'post'])!!}
                      <input type="hidden" name="id_producto" value="{{$producto->id_producto}}">
                      <div class="form-group">
                        <label class="control-label col-md-2" for="cantidad" style="padding:0px;">Cantidad:
                        </label>
                        <div class="col-md-5">
                          {!! Form::number('cantidad',null,['class'=>'form-control','id'=>'cantidad','min'=>'1','max'=>$producto->cantidad])!!}
                        </div>
                      </div>
                      
                      <div class="form-group pull-right">
                        <div class="col-md-5">
                          <button class="btn btn-primary">Agregar al carrito</button>
                        </div>
                      </div>
                    {!!Form::close()!!}
                </div>
              </div>
              @else
                 <div class="alert alert-danger" role="alert" >
                    <h4 class="text-center">Producto agotado</h4>
                  </div>
              @endif
            </div>
          </div>
      </div>
    </div>  
  </div>
  

@stop
@section('script')
  <script src="{{asset('assets/js/jquery.elevatezoom.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function(){
        $("#imagen").elevateZoom({
          zoomType        : "lens",
          lensShape : "round",
          lensSize    : 200
        });
    });
  </script>
  @if(Session::has('mensaje'))
      <?php $mensaje=Session::get('mensaje');?>

      <script>
        var mensaje="<?php echo $mensaje; ?>";
        new PNotify({
          title: 'Correcto',
          text: mensaje,
          type: 'success'
      });
      </script>
  @endif
@stop