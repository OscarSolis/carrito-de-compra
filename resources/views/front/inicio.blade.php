<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bienvenido</title>
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/agency.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/pnotify.custom.min.css')}}">

    

</head>

<body id="page-top" class="index">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="#page-top">Pilo Store</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias<span class="caret"></span></a>
                    <ul class="dropdown-menu lista">
                    	@foreach($categorias as $categoria)
    					<li><a href="{{url('listarProductos',$categoria->id_categoria)}}">{{$categoria->nombre}}</a></li>
    					<li role="separator" class="divider"></li>
                    	@endforeach
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"> ( {{$cantiProductos}} )</i><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu lista">
                        <li><a href="{{url('detalleCompra')}}">Ver detalle..</a></li>
                    </ul>
                </li>
                @if(\Auth::check())
                     <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-user fa-lg" aria-hidden="true"></i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{action('LoginController@logOut')}}">salir</a></li>
                          <li>
                            <a href="{{url('verCompras')}}">Ver Compras</a> 
                          </li>
                      </ul>
                @else
                    <li><a href="#modalLogin" class="portfolio-link" data-toggle="modal">Iniciar sesión</a></li>   
                    <li><a href="{{url('usuarios/create')}}">Registrarte</a></li>
                @endif      
                
                
            </ul>
        </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Bienvenido a Pilo Store</div>
                <div class="intro-heading">El lugar donde encontraras de todo!</div>
            </div>
        </div>
    </header>
    <div class="portfolio-modal modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Iniciar Sesion</h2>
                                            {!! Form::open(['url' => 'auth/login']) !!}
                          @if(Session::has('mensaje'))
                          <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            {{ Session::get('mensaje')}}
                          </div>
                          @endif

                          <fieldset>
                            <div class="row">
                              <div class="col-sm-12 col-md-10 col-md-offset-1">
                                <br>

                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                      <i class="fa fa-user" aria-hidden="true"></i>
                                    </span>
                                    {!! Form::text('email', null,['class'=>'form-control','id'=>'usuario', 'data-placement'=>'right']) !!}
                                  </div>
                                </div>
                                <br>
                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                      <i class="fa fa-lock" aria-hidden="true"></i>
                                    </span>
                                    {!! Form::password('password',['class'=>'form-control','id'=>'password', 'data-placement'=>'right']) !!}
                                  </div>
                                </div>
                                <br>
                                <div class="form-group">
                                  <input type="submit" class="btn btn-block btn-primary" value="ENTRAR">
                                </div>
                              </div>
                            </div>
                          </fieldset>
                          {!!form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Your Website 2014</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    

    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/classie.js')}}"></script>
    <script src="{{asset('assets/js/cbpAnimatedHeader.js')}}"></script>
    <script src="{{asset('assets/js/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('assets/js/contact_me.js')}}"></script>
    <script src="{{asset('assets/js/agency.js')}}"></script>
    <script src="{{asset('assets/js/pnotify.custom.min.js')}}"></script>
        @if(Session::has('mensaje'))
        <?php $mensaje=Session::get('mensaje');?>

        <script>
            var mensaje="<?php echo $mensaje; ?>";
            new PNotify({
                title: 'Correcto',
                text: mensaje,
                type: 'success'
            });
        </script>
        @endif

</body>

</html>
