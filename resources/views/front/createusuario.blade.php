@extends('front.masterFront')


@section('navbar')
	<nav class="navbar navbar-default " style="background-color: #222 !important;">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="{{url('/')}}">Pilo Store</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
              <li class="hidden">
                <a href="#page-top"></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias<span class="caret"></span></a>
                <ul class="dropdown-menu lista">
                	@foreach($categorias as $categoria)
					<li><a href="{{url('listarProductos',$categoria->id_categoria)}}">{{$categoria->nombre}}</a></li>
					<li role="separator" class="divider"></li>
                	@endforeach
                </ul>
                </li>
                    <!--<li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Categorias</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#team">Team</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>-->
                    <li class="dropdown active">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"> ( {{$cantiProductos}} )</i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{url('detalleCompra')}}">Ver detalle..</a></li>
                      </ul>
                   </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
@stop
@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="panel panel-default " >
    <div class="panel-heading text-center "><strong>Registrar usuario</strong></div>
    <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
                
                  <div class="x_content">
                   <br />
                   
                    {!! Form::open(['url' => 'usuarios','class'=>'form-horizontal form-label-left','files' => true]) !!}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('email',null,['class'=>'form-control col-xs-12 col-md-7','required'])!!}
                        </div>
                      </div>
                    

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('nombre',null,['class'=>'form-control col-xs-12 col-md-7','required'])!!}
                        </div>
                      </div>

                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contraseña
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::password('password',['class'=>'form-control col-xs-12 col-md-7','required'])!!}
                        </div>
                      </div>
  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dirección
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('direccion',null,['class'=>'form-control col-xs-12 col-md-7','required'])!!}
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Telefono
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('telefono',null,['class'=>'form-control col-xs-12 col-md-7','required'])!!}
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Numero de targeta
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          {!! Form::text('numero_targeta',null,['class'=>'form-control col-xs-12 col-md-7','required'])!!}
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group pull-right">
                        <div class="col-md-12">
                          <a href="{{url('a/')}}" class="btn btn-primary">Cancelar</a>
                          <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
                      </div>

                    {!! Form::close() !!}
                  </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>

@stop
