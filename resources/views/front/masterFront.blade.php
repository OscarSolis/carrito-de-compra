<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pilo Store</title>
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/agency.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/pnotify.custom.min.css')}}">
</head>

<body id="page-top" class="index">
    @yield('navbar')
    
  <div class="container-fluid">
    @yield('content')
  </div>  

  <div class="portfolio-modal modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Iniciar Sesion</h2>
                                            {!! Form::open(['url' => 'auth/login']) !!}
                          @if(Session::has('mensaje'))
                          <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            {{ Session::get('mensaje')}}
                          </div>
                          @endif

                          <fieldset>
                            <div class="row">
                              <div class="col-sm-12 col-md-10 col-md-offset-1">
                                <br>

                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                      <i class="fa fa-user" aria-hidden="true"></i>
                                    </span>
                                    {!! Form::text('email', null,['class'=>'form-control','id'=>'usuario', 'data-placement'=>'right']) !!}
                                  </div>
                                </div>
                                <br>
                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon">
                                      <i class="fa fa-lock" aria-hidden="true"></i>
                                    </span>
                                    {!! Form::password('password',['class'=>'form-control','id'=>'password', 'data-placement'=>'right']) !!}
                                  </div>
                                </div>
                                <br>
                                <div class="form-group">
                                  <input type="submit" class="btn btn-block btn-primary " value="ENTRAR">
                                </div>
                              </div>
                            </div>
                          </fieldset>
                          {!!form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Derechos reservados por Pilo Store</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </footer>
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/classie.js')}}"></script>
    <script src="{{asset('assets/js/cbpAnimatedHeader.js')}}"></script>
    <script src="{{asset('assets/js/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('assets/js/contact_me.js')}}"></script>
    <script src="{{asset('assets/js/agency.js')}}"></script>
    <script src="{{asset('assets/js/pnotify.custom.min.js')}}"></script>
    @yield('script')


</body>

</html>
