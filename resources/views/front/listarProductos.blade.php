@extends('front.masterFront')


@section('navbar')
	<nav class="navbar navbar-default " style="background-color:#222; !important;">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="{{url('/')}}">Pilo Store</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
              <li class="hidden">
                <a href="#page-top"></a>
              </li>
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias<span class="caret"></span></a>
                <ul class="dropdown-menu lista">
                	@foreach($categorias as $categoria)
					<li><a href="{{url('listarProductos',$categoria->id_categoria)}}">{{$categoria->nombre}}</a></li>
					<li role="separator" class="divider"></li>
                	@endforeach
                </ul>
                </li>
                   <!-- <li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Categorias</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#team">Team</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>-->
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"> ( {{$cantiProductos}} )</i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{url('detalleCompra')}}">Ver detalle..</a></li>
                      </ul>
                  </li>
                @if(\Auth::check())
                     <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-user fa-lg" aria-hidden="true"></i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{action('LoginController@logOut')}}">salir</a></li>
                          <li>
                            <a href="{{url('verCompras')}}">Ver Compras</a> 
                          </li>
                      </ul>
                  @else
                    <li><a href="#modalLogin" class="portfolio-link" data-toggle="modal">Iniciar sesión</a></li>   
                    <li><a href="{{url('usuarios/create')}}">Registrarte</a></li>
                  @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
@stop

@section('content')

  <div class="col-md-12" @if(count($productos)!=0) @else style="height:500px;" @endif>
    <div class="row">
       <ol class="breadcrumb" style="background:none;">
          <li><a href="{{url('/')}}">Inicio</a></li>
          <li>{{$nombreCategoria->nombre}}</li>
        </ol>
    </div>
    <div class="row">
    @if(count($productos)!=0)
      @foreach($productos as $producto)
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body">
              <h3 class="text-center">{{$producto->nombre}}</h3>
              <br>
              <img src="{{asset('imagenes')}}/{{$producto->imagen}}" class="img-responsive" style="margin:auto; height:300px !important; width:200px !important;" >
              
            </div>
             <div class="panel-footer" style="height:50px;">
                <h5 style="float:left;">Precio: ${{$producto->precio}}</h5>
                <a href="{{url('showProducto',$producto->id_producto)}}" class="btn btn-primary pull-right">Ver mas...</a>
             </div>
          </div>
        </div>
      @endforeach
    </div>  
      <div class="row">
        {!! $productos->render() !!}
      </div>  
    @else
      <div class="alert alert-danger" role="alert">
         <h4 class="text-center">No hay productos registrados</h4>
      </div>
    @endif  

    
  </div>
@stop