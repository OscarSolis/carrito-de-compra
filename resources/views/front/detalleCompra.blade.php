@extends('front.masterFront')


@section('navbar')
	<nav class="navbar navbar-default " style="background-color: #222 !important;">
      <div class="container">
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="{{url('/')}}">Pilo Store</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
              <li class="hidden">
                <a href="#page-top"></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias<span class="caret"></span></a>
                <ul class="dropdown-menu lista">
                	@foreach($categorias as $categoria)
					<li><a href="{{url('listarProductos',$categoria->id_categoria)}}">{{$categoria->nombre}}</a></li>
					<li role="separator" class="divider"></li>
                	@endforeach
                </ul>
                </li>
                   <!-- <li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Categorias</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#team">Team</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>-->
                    <li class="dropdown active">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"> ( {{$cantiProductos}} )</i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{url('detalleCompra')}}">Ver detalle..</a></li>
                      </ul>
                   </li>
                   @if(\Auth::check())
                     <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-user fa-lg" aria-hidden="true"></i><span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu lista">
                          <li><a href="{{action('LoginController@logOut')}}">salir</a></li>
                          <li>
                            <a href="{{url('verCompras')}}">Ver Compras</a> 
                          </li>
                      </ul>
                      
                   @else
                    <li><a href="#modalLogin" class="portfolio-link" data-toggle="modal">Iniciar sesión</a></li>   
                    <li><a href="{{url('usuarios/create')}}">Registrarte</a></li>
                  @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
@stop
@section('content')
    

  <div class="col-md-8 col-md-offset-2">
    <br><br>
    @if(count($productos)!=0)
      <h4 class="text-center">Productos agregados al carrito</h4>
      <br>
      <div class="table-responsive" style="height:400px">
        <table class="table table-striped table-hover">
          <thead>
            <tr  class="btn-primary">
              <th>Producto</th>
              <th>Cantidad</th>
              <th>Precio</th>
              <th>Subtotal</th>
              <th>Eliminar</th>
             </tr> 
          </thead>
          <tbody>
            <?php $cant=0; $total = 0;?>
            @foreach($productos as $producto)
              <tr data-id="{{$producto->rowid}}">
                <td><strong>{{$producto->name}}</strong></td>
                <td>{{$producto->qty}}</td>
                <td>${{number_format($producto->price,2)}}</td>
                <td>${{number_format($producto->subtotal,2)}}</td>
                <td><a href="{{url('eliminarProducto',$producto->rowid)}}" class="btn btn-warning">X</a></td>
                <?php $cant += $producto->qty; $total += $producto->subtotal;?>
              </tr>  
            @endforeach  
              <tr class="">
                <td><strong>Total</strong></td>
                <td><strong>{{$cant}}</strong></td>
                <td><strong>${{number_format($total,2)}}</strong></td>
                <td><strong>${{number_format($total,2)}}</strong></td>
                <td></td>
              </tr>
          </tbody>
        </table>
        <a href="#" id="pagar" class="btn btn-primary pull-right">Pagar</a>
      </div>   
    @else
      <div class="col-md-12" style="height:400px">
        <div class="alert alert-danger" role="alert" >
           <h4 class="text-center">No hay productos en el carrito</h4>
        </div>  
      </div>  
    @endif

  </div>
  

@stop
@section('script')
  <script src="{{asset('assets/js/jquery.elevatezoom.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/js/pnotify.confirm.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function(){
        $("#imagen").elevateZoom({
          zoomType        : "lens",
          lensShape : "round",
          lensSize    : 200
        });


        $('#pagar').click(function(){
         (new PNotify({
                    title: 'Confirmacion!',
                    text: '¿Esta seguro que desea finalizar su compra?',
                    icon: 'glyphicon glyphicon-question-sign',
                    hide: false,
                    confirm: {
                        confirm: true,
                        buttons:[{text:'aceptar'},{text:'cancelar'}]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    history: {
                        history: false
                    },
                    addclass: 'stack-modal',
                    stack: {'dir1': 'down', 'dir2': 'right', 'modal': false}
                })).get().on('pnotify.confirm', function(){
                    window.location = 'finalizarCompra';
                }).on('pnotify.cancel', function(){
                    
                }); 
        });
    });
  </script>
  @if(Session::has('mensaje'))
      <?php $mensaje=Session::get('mensaje');?>

      <script>
        var mensaje="<?php echo $mensaje; ?>";
        new PNotify({
          title: 'Correcto',
          text: mensaje,
          type: 'success'
      });
      </script>
  @endif
@stop