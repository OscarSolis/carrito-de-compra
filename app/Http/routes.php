<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'FrontController@index');

Route::get('listarProductos/{id_categoria}','FrontController@listarProductos');

Route::get('showProducto/{id_producto}','FrontController@showProducto');

Route::post('addCart','FrontController@addCart');

Route::get('detalleCompra','FrontController@detalleCompra');

Route::get('eliminarProducto/{idRow}','FrontController@eliminarProducto');

Route::group(['middleware' => ['auth']], function(){
	Route::get('finalizarCompra','FrontController@finalizarCompra');

	Route::get('verCompras','FrontController@verCompras');
});



/*------------Rutas para login comite------------*/
Route::get('auth/login', 'LoginController@showLogin'); // Mostrar el formulario de login

Route::post('auth/login', 'LoginController@postLogin'); // Verificar datos

Route::get('auth/logout', 'LoginController@logOut'); // Finalizar sesión

Route::resource('usuarios','UsuarioController');

Route::group(['prefix' => 'administrador','middleware' => 'auth'], function(){
	Route::resource('categoria', 'CategoriaController');
	Route::resource('producto', 'ProductoController');
	Route::get('home',function(){
		return view('administrador.inicio');
	});
});
