<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB;
use App\Usuario;
use App\Categoria;
use Cart;
class UsuarioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cantiProductos = Cart::count();
		$categorias = Categoria::select('id_categoria','nombre')->get();
		return view('front.createusuario',compact('categorias','cantiProductos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
	  	$user = new Usuario();
	  	$user->id          = $request->email;
	  	$user->nombre         = $request->nombre;
	  	$user->password       = \Hash::make($request->password);
	  	$user->direccion      = $request->direccion;
	  	$user->telefono       = $request->telefono;
	  	$user->numero_targeta = $request->numero_targeta;
	  	$user->rol            = 'cliente';
	  	$user->save();
	  	return redirect('auth/login')->with('mensaje','Inicie sesion con su nuevo usuario');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
