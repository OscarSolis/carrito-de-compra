<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Producto;
use App\Categoria;
use DB;
class ProductoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$productos = Producto::paginate(10);
		return view('administrador.indexProductos',compact('productos'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categorias = Categoria::select('id_categoria','nombre')->get();
		return view('administrador.createProducto',compact('categorias'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
			
		$producto = new Producto();
		$file = $request->file('imagen');
		$nombre = date('m-d-i-s')."-".$file->getClientOriginalName();
		$producto->id_categoria   = $request->id_categoria;
		$producto->nombre      = $request->nombre;
		$producto->precio      = $request->precio;
		$producto->cantidad    = $request->cantidad;
		$producto->descripcion = $request->descripcion;
		$producto->imagen      = $nombre;
		$producto->save();
       	\Storage::disk('local')->put($nombre,  \File::get($file));
		return redirect()->back()->with('mensaje','Producto registrado correctamente!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$producto = Producto::find($id);
		$categorias = Categoria::select('id_categoria','nombre')->get();
		return view('front.showProducto',compact('producto','categorias'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$producto = Producto::find($id);

		$categorias = DB::table('categorias')->lists('nombre','id_categoria');
		return view('administrador.editProducto',compact('producto','categorias'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		
		$producto = Producto::find($id);
		$producto->fill($request->all());
		$producto->save();
		return redirect('administrador/producto')->with('mensaje','Actualizacion correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
