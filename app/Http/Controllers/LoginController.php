<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Usuario;
use Illuminate\Http\Request;

class LoginController extends Controller {

	 public function showLogin()
    {
        // Verificamos si hay sesión activa
        if (\Auth::check())
        {
            // Si tenemos sesión activa mostrará la página de inicio dependiendo de su rol
            $usuario=\Auth::user();
            if($usuario->rol=='administrador'){
                return \Redirect::to('administrador/home');
            }else{
                if($usuario->rol=='cliente'){
                    return \Redirect::to('detalleCompra');
                }
            }
            
        }
        // Si no hay sesión activa mostramos el formulario
        return view('login.login');
    }
 
    public function postLogin()
    {
        // Obtenemos los datos del formulario
        $data = [
            'id' => \Input::get('email'),
            'password' => \Input::get('password')
        ];

        $usr = Usuario::find($data['id']);
        if($usr!=null){
            // Verificamos los datos
            if (\Auth::attempt($data)){
                $usuario = \Auth::user();
                    if ($usuario->rol=='cliente') {
                       return \Redirect::to('/');  
                    }else{
                        if($usuario->rol=='administrador'){
                            return \Redirect::to('detalleCompra');
                        }
                    }
                    // Si nuestros datos son correctos mostramos la página de inicio
                    
                }else{
                    // Si los datos no son los correctos volvemos al login y mostramos un error
                    return \Redirect::back()->with('mensaje', 'Datos Invalidos')->withInput();        
                }
            
        }else{
            // Si los datos no son los correctos volvemos al login y mostramos un error
            return \Redirect::to('usuarios/create');        
        }
    }
 
    public function logOut()
    {
        // Cerramos la sesión
        \Auth::logout();
        // Volvemos al login y mostramos un mensaje indicando que se cerró la sesión
        return \Redirect::to('/');
    }


}
