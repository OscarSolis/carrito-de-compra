<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Categoria;
use App\Producto;
use Cart;
use App\Compra;
use App\DetalleCompra;
class FrontController extends Controller {

	
	public function index(){
		$cantiProductos = Cart::count();
		$categorias = Categoria::all();
		return view('front.inicio',compact('categorias','cantiProductos'));
	}

	public function listarProductos($id_categoria){
		$cantiProductos = Cart::count();
		$productos = Producto::where('id_categoria','=',$id_categoria)->paginate(15);
		$categorias = Categoria::all();
		$nombreCategoria = Categoria::find($id_categoria);
		return view('front.listarProductos',compact('productos','categorias','nombreCategoria','cantiProductos'));
	}


	public function showProducto($id)
	{
		$cantiProductos = Cart::count();
		$producto = Producto::find($id);
		$categorias = Categoria::select('id_categoria','nombre')->get();
		return view('front.showProducto',compact('producto','categorias','cantiProductos'));
	}

	public function detalleCompra(){
		$productos = Cart::content();
		$cantiProductos = Cart::count();
		$categorias = Categoria::select('id_categoria','nombre')->get();
		return view('front.detalleCompra',compact('productos','cantiProductos','categorias'));
	}

	public function addCart(Request $request){

		$producto = Producto::find($request->id_producto);
		Cart::add(['id' => $producto->id_producto, 'name' => $producto->nombre, 'qty' => $request->cantidad, 'price' =>$producto->precio]);
		$producto->cantidad = $producto->cantidad - $request->cantidad;
		$producto->save();
		return redirect()->back()->with('mensaje','Producto agregado correctamente al carrito!');
		
	}


	public function finalizarCompra(){

		$compra = new Compra();
		$compra->id_user = \Auth::user()->id;
		$compra->save();
		foreach (Cart::content() as $articulo) {
			$detalleCompra = new DetalleCompra();
			$detalleCompra->id_compra = $compra->id_compra;
			$detalleCompra->nombre    = $articulo->name;
			$detalleCompra->cantidad  = $articulo->qty;
			$detalleCompra->precio    = $articulo->subtotal;
			$detalleCompra->save();
		}
		Cart::destroy();
		return redirect('/')->with('mensaje','Compra realizada correctamente!');
	}

	public function eliminarProducto($idRow){


		Cart::remove($idRow);
		return redirect('detalleCompra')->with('mensaje','Compra realizada correctamente!');
	}


	public function verCompras(){
		$cantiProductos = Cart::count();
		$categorias = Categoria::select('id_categoria','nombre')->get();
		$compras = Compra::where('id_user','=',\Auth::user()->id)->get();
		return view('front.verCompras',compact('cantiProductos','categorias','compras'));
	}
	
}
