<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model {

	protected $table = 'users';

	protected $primaryKey = 'id';

	protected function productos(){
		return $this->hasMany('App\Producto','id_categoria');
	}


	protected $fillable=[
    	'id',
    	'nombre',
    	'email',
    	'password',
    	'direccion',
    	'telefono',
    	'numero_targeta',
    	'rol'
    ];

}
