<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {

	protected $table = 'categorias';

	protected $primaryKey = 'id_categoria';

	protected function productos(){
		return $this->hasMany('App\Producto','id_categoria');
	}


	protected $fillable=[
    	'id_categoria',
    	'nombre'
    ];

}
