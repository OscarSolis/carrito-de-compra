<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model {

	protected $table = 'productos';

	protected $primaryKey = 'id_producto';

    public function categoria(){
        return $this->belongsTo('App\Categoria','id_categoria');
    }

	protected $fillable=[
    	'id_producto',
    	'id_categoria',
    	'nombre',
    	'precio',
    	'cantidad',
    	'descripcion',
    	'imagen'
    ];

}
