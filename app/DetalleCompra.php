<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model {

	protected $table = 'compra_user';

	protected $primaryKey = 'id_detalle';

	 public $timestamps = false;


	protected $fillable=[
    	'id_compra',
    	'nombre',
    	'cantidad',
    	'precio'
    ];


}
