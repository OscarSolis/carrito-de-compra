<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model {

	protected $table = 'compras';

	protected $primaryKey = 'id_compra';


	public function detalle(){
		return $this->hasMany('App\DetalleCompra','id_compra');
	}

	protected $fillable=[
    	'id_compra',
    	'id_user',
    ];


}
